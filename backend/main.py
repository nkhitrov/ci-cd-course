from fastapi import FastAPI
from os import environ
import logging
import asyncpg


SECRET_KEY = environ.get("SECRET_KEY", "secret")
DEBUG = environ.get("DEBUG", False)

_level = logging.DEBUG if DEBUG else logging.INFO
logging.basicConfig(level=_level)


app = FastAPI()

logger = logging.getLogger(__name__)

@app.get("/api/system")
async def get_system_info():
    logger.debug(f"secret key {SECRET_KEY}")

    users = await _get_users()

    return {"status": "ok", "secret_key": SECRET_KEY, "users": users}

async def _get_users():
    try:
        conn = await asyncpg.connect('postgresql://postgres@postgres/test')
    except Exception as error:
        logger.warning(f"failed to connect to postgres with reason {error}")
        return []

    try:
        users = await conn.fetchval('select * from users')
        logger.debug(f"users {users}")
        await conn.close()
    except Exception as error:
        logger.warning(f"failed to get users with reason {error}")
        return []
